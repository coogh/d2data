Ancien et en exclusivit� pour les Angels of Fire, Neptune est fier de vous pr�senter www.d2data.net.

Ancien ?
En effet le site n'existe plus.

Alors pourquoi le ressuciter ?
Ce site nous permettait de d�terminer o� etaient susceptibles d'�tre dropp�s certains objets.
Vous avez toujours voulu savoir � partir d'o� on peut dropper une amulette Tal ?
-1- Cliquer sur items de sets puis Tal Rasha
-2- Un tableau apparait pour tous les objets du set.
-3- Ce tableau vous apprendra que Mephisto en normal peut dropper l'amu.

Les donn�es fournies sont elles fiables ?
Oui � 100% : Les donn�es fournies sont extraites des fichiers .mpq de Diablo II.

Bugs connus :
	1/ Le volet de gauche du site (recherches) ne fonctionne pas en version aspir�e.
	2/ Sur les pages du site affichant un tableau, les liens du haut de page peuvent ne pas fonctionner et vous r�orienter sur une page d'erreur vous indiquant que le lien en question n'a pas �t� captur�. Pour revenir � l'accueil du site juste cliquer sur "Home" dans le volet gauche.

Installation :

1- D�compresser le fichier D2Data.zip.Si vous lisez ce texte, vous avez trouv� tout seul la 1ere �tape. Normal, vous �tes un AoF, donc vous roxxez du poulet !!

2- D�placez le dossier "D2Data" � l'endroit de votre choix (Dossier "Mes documents" ou "Diablo II" ou n'importe quoi d'autre)

3- Double-cliquer sur le fichier "Index" ou "Index.html" situ� dans le dossier D2Data et le site se lance m�me si votre connexion Internet ne fonctionne pas.

4- Sur la fen�tre d'accueil, ouvrez le menu "Favoris" puis "Ajouter aux favoris" si vous souhaitez un acc�s rapide au site. Vous pouve aussi depuis le dossier "D2Data" cliquer-droit sur "Index" (Ou "Index.html" puis "Envoyer vers" et enfin "Bureau - Creer un raccourci"